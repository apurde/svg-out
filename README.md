# SVG Out add-in
The SVG Out add-in is intended to bring broader SVG functionality to Atlassian Confluence. Confluence natively supports the output of some SVGs (with some restrictions). As the SVGs are embedded as image your will loose the functionality of embedded links however. This add-in comes with some additional features like crop, pan&zoom and link replacement.

## License
Please refer to our [Source code license agreement](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement)

## Manual
Please refer to the Wiki pages of this repository.

## Branches
The sources contain two branches. As the "dev" branch is work in progress you should only use the sources of the master branch.

## Building

Run the following commands on the sources (you need Atlassian's SDK for that):

```
atlas-clean
atlas-package
```  
