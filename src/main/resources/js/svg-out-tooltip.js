// when the content is ready
AJS.$(window).on("load", function() {
	
	// the SVG name space; we need this for appending the node
	var svgns = "http://www.w3.org/2000/svg";

	// get all svg-outs on the page
	var svgouts = document.getElementsByClassName("svg-out");

	// loop through all svg-outs
	for(var s = 0; s < svgouts.length; s ++) {
	
		// in case the attribute is true
		if(svgouts[s].getAttribute("IEToolTips") == "true") {

			// get all links in the SVG
			var svgDoc = svgouts[s].firstElementChild.contentDocument.documentElement;
			var links = svgDoc.getElementsByTagName("a");
			
			// loop through all links
			for(var l = 0; l < links.length; l++) {
			
				// in case the link contains the xlink:title attribute
				if(links[l].hasAttribute("xlink:title")) {
				
					// build a new node <title>xlink:title</title> in the svg namespace (!!!)
					var title = links[l].getAttribute("xlink:title");
					var para = document.createElementNS(svgns, "title");
					var node = document.createTextNode(title);
					para.appendChild(node);
					
					// append the new node to the link
					links[l].appendChild(para);
				}
			}
		}
		
		
	}
});
