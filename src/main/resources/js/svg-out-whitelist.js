AJS.toInit(function () {
	AJS.$(".svg-out-security-check").unbind("click");
	AJS.$(".svg-out-security-check").bind("click", function() {
		var sha = AJS.$(this).data("sha");
		var token = AJS.$(this).data("token");
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/svgout/1.0/whitelist?sha=" + sha + "&token=" + token,
			type: "PUT",
			dataType: "json",
			success: function(result) {
				AJS.flag({
				    type: 'success',
				    body: AJS.I18n.getText("de.edrup.confluence.plugins.svg-out.message.whitelist.sucess")
				});
			},
			error: function(request, status, error) {
				AJS.flag({
				    type: 'error',
				    body: AJS.I18n.getText("de.edrup.confluence.plugins.svg-out.message.whitelist.error")
				});
			}
		});
	});
});
