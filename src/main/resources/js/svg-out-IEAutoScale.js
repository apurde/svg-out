function IEAutoScale() {
	
	// is this IE?
	var isIE = /*@cc_on!@*/false || !!document.documentMode;
	
	// if yes
	if(isIE) {
	
		// get the svg-out on the page
		var svgout = document.getElementsByClassName("svg-out")[0];

		// get the SVG itself
		var svg = svgout.firstElementChild;
		
		// in case the width is a % value
		if(svg.getAttribute("width").indexOf("%") > 0) {
		
			// apply the so called padding-bottom fix according
			// https://css-tricks.com/scale-svg/
			// remark: we need to maintain overflow:hidden as we still want to crop
			 
			var width = svg.getAttribute("width");
			var style = svg.getAttribute("style");
			var viewBoxValues = svg.getAttribute("viewBox").split(' ', 4);
			style = style + ";width:" + width + ";height:1px;padding-bottom:calc(" + width + "*" + viewBoxValues[3] + "/" + viewBoxValues[2] + ");"; 
			svg.setAttribute("style", style);
			svg.removeAttribute("width");
			svg.setAttribute("preserveAspectRatio", "xMidYMin slice");
		}
	}
};