AJS.toInit(function () {

	// this functions checks whether the editor is available (we are in edit mode)
	var checkForEditor = function() {
		if(AJS.Rte) {
			if(AJS.Rte.getEditor()) {
				if(AJS.Rte.getEditor().dom) {
					if(AJS.Rte.getEditor().dom.getRoot()) {
						observingEditor = true;
						console.log('svgOutBodyObserver added!');
						svgOutBodyObserver.observe(AJS.Rte.getEditor().dom.getRoot(), svgOutBodyObserverConfig);
					}
				}
			}
		}
	};
	
	// we check for the editor whenever the DOM changes
	var svgOutEditorObserver = new MutationObserver(function() {
		if(observingEditor == false) {
			checkForEditor();
		}
	});					
			
	
	// check all changes when editing
	var svgOutBodyObserver = new MutationObserver(function(mutations) {
		
		// loop through all mutations reported
		mutations.forEach(function(mutation) {
		
			// loop through all added nodes
  			for(n = 0; n < mutation.addedNodes.length; n++) {
  			
  				// check whether it is a svg-out macro
  				if(AJS.$(mutation.addedNodes[n]).attr('data-macro-name') == 'svg-out') {
  				
  					// check whether it has an empty body
  					var body = AJS.$(mutation.addedNodes[n]).find('.wysiwyg-macro-body:first');
  					if(AJS.$(body).html() == "<p><br></p>") {
       					console.log("New SVG Out macro with empty body detected!");
       					
       					// fill the empty body with our hint
       					var bodyHint = AJS.I18n.getText('de.edrup.confluence.plugins.svg-out.bodyHint');
	       				AJS.$(body).html("<p>" + bodyHint + "</p>");
	       			}
       			}
       		}
        });
	});
	
	// options for observing the body of the editor
	var svgOutBodyObserverConfig = {
        childList: true,
        subtree: true
	};
	
	// options for observing whether the editor is present
	var svgOutEditorObserverConfig = {
        childList: true,
        subtree: true
	};
	
	// init variables
	var observingEditor = false;
	
	// check whether we already have the editor
	checkForEditor();
	
	// observe the page once the editPageLink button has been pressed
	$('#editPageLink').click(function () {
		svgOutEditorObserver.observe(document.body, svgOutEditorObserverConfig);
		});
});