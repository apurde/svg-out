// when the content is ready
AJS.$(window).on("load", function() {
	
	// get all svg-outs on the page
	var svgouts = document.getElementsByClassName("svg-out");

	// loop through all svg-outs
	for(var s = 0; s < svgouts.length; s ++) {
		var css = "";
		if(svgouts[s].getAttribute("linkHighlight") == "true") {
			css += "a {outline-style:dotted;outline-color:gray;outline-offset:-2px;} ";
		}
		if(svgouts[s].getAttribute("linkHoverHighlight") == "true") {
			css += "a:hover {opacity:0.5;}";
		}
		var style= document.createElementNS('http://www.w3.org/2000/svg', 'style');
		style.type = 'text/css';
		style.appendChild(document.createTextNode(css));
		var svgDoc = svgouts[s].firstElementChild.contentDocument.documentElement;
		svgDoc.appendChild(style);
	}
});
