package de.edrup.confluence.plugins;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.atlassian.confluence.index.attachment.AttachmentTextExtractor;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.util.io.InputStreamSource;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class SVGTextExtractorV2 implements AttachmentTextExtractor {
	
	private final AttachmentManager attachmentManager;
	
	private static final Logger log = LoggerFactory.getLogger(SVGTextExtractorV2.class);
	
	public static final String[] MIME_TYPES = {"image/svg+xml"};
	public static final String[] FILE_EXTS = {"svg", "svgz"};

	 
    @Autowired
    public SVGTextExtractorV2(@ComponentImport AttachmentManager attachmentManager) {
        this.attachmentManager = attachmentManager;
    }
 
    
    @Override
    public List<String> getFileExtensions() {
        return Arrays.asList(FILE_EXTS);
    }
 
    
    @Override
    public List<String> getMimeTypes() {
        return Arrays.asList(MIME_TYPES);
    }
    
    
    @Override
    public Optional<InputStreamSource> extract(Attachment attachment) {
        try {
        	InputStream is = attachmentManager.getAttachmentData(attachment);
            if (is != null) {
                String text = extractText(is, attachment);
                InputStreamSource inputStreamSource = new InputStreamSource() {
                    @Override
                    public InputStream getInputStream() {
                        return new ByteArrayInputStream(text.getBytes(StandardCharsets.UTF_8));
                    }
                };
                return Optional.of(inputStreamSource);
            }
            return Optional.empty();
        } catch (Exception e) {
        	log.error(e.toString());
        	return Optional.empty();
        }
    }
    
    
    private String extractText(InputStream is, Attachment attachment) {
		try {
			// read the attachment into a String
			String svgS = "";
			try {
				if(attachment.getFileName().endsWith(".svgz")) {
					GZIPInputStream gzi = new GZIPInputStream(is);
					svgS = IOUtils.toString(gzi, "UTF-8");
				}
				else {
					svgS = IOUtils.toString(is, "UTF-8");
				}
			}
			catch(IOException e) {
				return "";
			}
			
			// remove all line feeds (line feeds prevent the matcher from working as we want it)
			svgS = svgS.replace("\n", "").replace("\r", "");
			
			// init our text found string
			String textFound = "";
			
			// search for all text nodes
			Pattern p = Pattern.compile("< *text.*?>.*?< */text *>");
			Matcher m = p.matcher(svgS);
			while(m.find()) {
				String match = m.group(0);
				
				// remove the pre- and post-parts and all tspans
				match = match.replaceAll("< *text.*?>", "").replaceAll("< */text *>", "").replaceAll("< *tspan.*?>", " ").replaceAll("< */tspan *>", "");
				
				// remove multiple white spaces
				match = match.replaceAll(" +", " ");
				
				// add the text part to our list
				if(match.length() > 0) { textFound = textFound + match.trim() + " / "; }
			}
	
			// return the text found
			return textFound;
	    }
	    catch(Exception e) {
	    	log.error(e.toString());
	    	return "";
	    }
    }
}
