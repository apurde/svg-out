package de.edrup.confluence.plugins;

import java.util.concurrent.TimeUnit;
import javax.inject.Inject;
import javax.inject.Named;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

@Named
public class SVGObjectCache {

    private final Cache<String, String> svgObjectContentCache;
    private final Cache<String, Boolean> tokenCache;
    private final I18nResolver i18n;

    @Inject
    public SVGObjectCache(@ComponentImport CacheManager cacheManager, @ComponentImport I18nResolver i18n) {
        this.i18n = i18n;

        // set up the caches
        svgObjectContentCache = cacheManager.getCache("SVG Out object cache", null,
                new CacheSettingsBuilder().remote().expireAfterAccess(5, TimeUnit.MINUTES).maxEntries(400).build());
        tokenCache = cacheManager.getCache("SVG Out token cache", null,
                new CacheSettingsBuilder().remote().expireAfterWrite(5, TimeUnit.MINUTES).build());
    }

    public void add(String id, String svgObject) {
        svgObjectContentCache.put(id, svgObject);
    }

    public String get(String id) {
        if (svgObjectContentCache.containsKey(id)) {
            return svgObjectContentCache.get(id);
        }
        return i18n.getText("de.edrup.confluence.plugins.svg-out.message.negativeCache");
    }

    public void addToken(String token, String sha) {
        tokenCache.put(token.concat(sha), true);
    }

    public boolean isTokenValid(String token, String sha) {
        if (tokenCache.get(token.concat(sha)) != null) {
            tokenCache.remove(token.concat(sha));
            return true;
        }
        return false;
    }
}