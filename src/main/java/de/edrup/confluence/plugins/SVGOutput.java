package de.edrup.confluence.plugins;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.URL;
import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.inject.Inject;
import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.HtmlUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.Format;
import com.atlassian.confluence.content.render.xhtml.macro.annotation.RequiresFormat;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.GlobalSettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.coverity.security.Escape;
import com.atlassian.confluence.macro.xhtml.MacroManager;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;


public class SVGOutput implements Macro {
	
	private final PageManager pageMan;
	private final AttachmentManager attachmentMan;
	private final PermissionManager permissionMan;
	private final BandanaManager bandanaMan;
	private final I18nResolver i18n;
	private final XhtmlContent xhtmlUtils;
	private final GlobalSettingsManager settingsMan;
	private final MacroManager macroMan; 
	private final PluginAccessor pluginAcc; 
	private final UserAccessor userAcc;
	
	private Document svgD;
	private Element svgE;
	private String svgS;
	private String errorMsg;
	private final Cache<String,Boolean> SHACache;
	private SVGObjectCache svgObjectCache;
	
	private static final Logger log = LoggerFactory.getLogger(SVGOutput.class);
	
	private static String pngOutputFor = "pdf word feed email html_export";


	@Inject
	public SVGOutput(SVGObjectCache svgObjectCache, @ComponentImport PageManager pageMan, @ComponentImport AttachmentManager attachmentMan,
			@ComponentImport PermissionManager permissionMan, @ComponentImport BandanaManager bandanaMan,
			@ComponentImport I18nResolver i18n, @ComponentImport XhtmlContent xhtmlUtils, @ComponentImport GlobalSettingsManager settingsMan,
			@ComponentImport CacheManager cacheMan, @ComponentImport MacroManager macroMan, @ComponentImport PluginAccessor pluginAcc,
			@ComponentImport UserAccessor userAcc) {
		this.pageMan = pageMan;
		this.attachmentMan = attachmentMan;
		this.permissionMan = permissionMan;
		this.bandanaMan = bandanaMan;
		this.settingsMan = settingsMan;
		this.macroMan = macroMan;
		this.i18n = i18n;
		this.xhtmlUtils = xhtmlUtils;
		this.pluginAcc = pluginAcc;
		this.svgObjectCache = svgObjectCache;
		this.svgD = null;
		this.svgE = null;
		this.svgS = "";
		this.errorMsg = "";
		this.userAcc = userAcc;
		
		SHACache = cacheMan.getCache("SVG Out SHA cache", null,
			new CacheSettingsBuilder().remote().expireAfterAccess(30, TimeUnit.DAYS).build());
	}

	
	@Override
	@RequiresFormat(Format.Storage)
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
		
		// determine some values we need to for the correct input and output paths
		boolean pngOutput = pngOutputFor.contains(conversionContext.getOutputType()) ? true : false;
		boolean scrollOutput = pngOutput && isScrollPDFActive();
		boolean injected = parameters.containsKey("name") ? false : true;
		
		// prepare the body
		// remark: normally the macro gets a converted body - however we need to control this step in case of PDF or Word output
		// remark: the way we used (via convertStorageToView) also for the first branch does not work in case of object output - no idea why 
		if(injected) {
			DefaultConversionContext changedContext = new DefaultConversionContext(pageMan.getPage(conversionContext.getEntity().getId()).toPageContext());
			try {
				final List<MacroDefinition> macros = getMacroDefs(bodyContent, changedContext);
				Macro m = macroMan.getMacroByName(macros.get(0).getName());
				bodyContent = m.execute(macros.get(0).getParameters(), macros.get(0).getBodyText(), changedContext);
			}
			catch (Exception e) {
				return i18n.getText("de.edrup.confluence.plugins.svg-out.message.injectedBody");
			}
		}
		else {	
			try {
				bodyContent = xhtmlUtils.convertStorageToView(bodyContent, conversionContext);
			}
			catch (Exception e) {
				return e.toString();
	        }	
		}
		
		// get the SVG document from the attachment
		if(!injected) {
			if(!documentFromAttachment(parameters, conversionContext)) {
				return errorMsg;
			}
		}	
		
		log.debug(bodyContent);
		
		// get the SVG document from the attachment
		if(!injected) {
			if(!documentFromAttachment(parameters, conversionContext)) {
				return errorMsg;
			}
		}
		
		// get the SVG document from the body
		else {
			
			// we can't do preview for injected SVGs
			if(conversionContext.getOutputType().equals("preview")) {
				return i18n.getText("de.edrup.confluence.plugins.svg-out.message.injected.preview");
			}
						
			// create the DOM from the body
			if(!documentFromBody(bodyContent)) {
				return errorMsg;
			}
		}
		
		// check whether the DOM element is of tag SVG
		if(!svgD.getDocumentElement().getTagName().toLowerCase().equals("svg")) {
			return i18n.getText("de.edrup.confluence.plugins.svg-out.message.topLevel");
		}
		
		// read the configuration
		SVGOutConfiguration conf = new SVGOutConfiguration();
		conf.readConfiguration(bandanaMan);
		
		// perform the security check only in case it is active and we are not handling an injected SVG
		if(conf.getActive() && !injected) {
			
			// get the svg as text
			if(!domToString()) {
				return errorMsg;
			}
			
			// calculate the MD5 of the SVG (legacy)
			SVGSecurityCheck sc = new SVGSecurityCheck();
			String svgMD5 = sc.MD5Calc(svgS);
			
			// calculate the SHA3-256 of the SVG
			String svgSHA = sc.SHA256Calc(svgS);
			String svgSHACacheKey = svgSHA.concat("2130");
			
			// perform the security only in case the SVG is not white-listed and not in the cache
			if(!conf.getWhitelist().contains(svgMD5) && !conf.getWhitelist().contains(svgSHA) && ((SHACache.get(svgSHACacheKey) == null) || conf.getWhitelist().contains("forceWhiteListingOfAllSVGs"))) {
				
				// in case the performed security check is negative or white listing is enforced
				if(conf.getWhitelist().contains("forceWhiteListingOfAllSVGs") || !(sc.doCheck(svgS, conf.getWhiteRegEx()))) {
					ConfluenceUser user = AuthenticatedUserThreadLocal.get();
					String token = "";
					if(permissionMan.isSystemAdministrator(user) || userAcc.getGroupNames(user).contains(conf.getWhiteListGroup())) {
						token = UUID.randomUUID().toString();
						svgObjectCache.addToken(token, svgSHA);
					}
					return i18n.getText("de.edrup.confluence.plugins.svg-out.message.securityCheck").replace("$sha", svgSHA).replace("$token", token);
				}
				else {
					SHACache.putIfAbsent(svgSHACacheKey, true);
				}
			}
		}
				
		// get the scale, cut and border parameters
		String pScaleX = Escape.html(parameters.containsKey("scaleX") ? parameters.get("scaleX") : "undefined");
		String pScaleY = Escape.html(parameters.containsKey("scaleY") ? parameters.get("scaleY") : "undefined");
		String maxWidth = Escape.html(parameters.containsKey("maxWidth") ? parameters.get("maxWidth") : "");
		String pLeft = Escape.html(parameters.containsKey("left") ? parameters.get("left") : "0%");
		String pTop = Escape.html(parameters.containsKey("top") ? parameters.get("top") : "0%");
		String pRight = Escape.html(parameters.containsKey("right") ? parameters.get("right") : "100%");
		String pBottom = Escape.html(parameters.containsKey("bottom") ? parameters.get("bottom") : "100%");
		String pBorder = Escape.html(parameters.containsKey("border") ? parameters.get("border") : "false");
		String rasterResolution = Escape.html(parameters.containsKey("rasterResolution") ? parameters.get("rasterResolution") : "100%");
		
		// get the top most element which is <svg>
		svgE = svgD.getDocumentElement();
		
		// apply zoom and cut
		if(!zoomAndCut(pScaleX, pScaleY, pLeft, pTop, pRight, pBottom, pngOutput, rasterResolution)) {
			return errorMsg;
		}
					
		// replace links and add tool tips
		if(!injected && !pngOutput) {
			String urlPath = (conversionContext.getEntity() != null) ? conversionContext.getEntity().getUrlPath() : "";
			linkAndTooltipHandler(bodyContent, urlPath);
		}
				
		// transfer the manipulated DOM to a string again after our changes
		if(!domToString()) {
			return errorMsg;
		}
		
		// in case we are generating a PDF, Word or Email use Batik to generate a PNG out of the document
		String svgOutput = "";
		if(pngOutput && !scrollOutput) {
			
			// Bug #18: Word export does not like images with %-width
			if(conversionContext.getOutputType().equals("word")) {
				pScaleX = pScaleX.replace("%%", "%");
			}
			
			svgOutput = svg2Img(pBorder.equals("true"), pScaleX, rasterResolution);
		}
		
		// wrap the SVG in an object in case we do not use the PNG version 
		else {
						
			// calculate the MD5 of the changed MD5 as ID
			// remark: we append the version number of the plug-in to allow handling changes
			String objectId = new SVGSecurityCheck().MD5Calc(svgS) + ".svg"; 
			
			// add the SVG to the cache
			svgObjectCache.add(objectId, svgS);
			
			// determine the style of the object
			String objectStyle = "style='";
			if(pScaleX.contains("%%")) {
				objectStyle = objectStyle + "width:" + pScaleX.replace("%%", "%") + ";";
				if(maxWidth.length() > 0) {
					objectStyle = objectStyle + "max-width:" + maxWidth + ";";
				}
			}
			if(pBorder.equals("true")) {
				objectStyle = objectStyle + "border:1px solid black";
			}
			objectStyle = objectStyle + "'";
			
			// build the object
			if(!scrollOutput) {
				svgOutput = "<object " + objectStyle + " type='image/svg+xml' data='" + settingsMan.getGlobalSettings().getBaseUrl() +
					"/rest/svgout/1.0/getsvg?id=" + objectId + "'></object>";
			}
			else {
				svgOutput = "<img src='" +  settingsMan.getGlobalSettings().getBaseUrl() + "/rest/svgout/1.0/getsvg?id=" + objectId + "'></img>";
			}
		}
		
		// get pan & zoom parameters
		String panAndZoom = Escape.html(parameters.containsKey("panAndZoom") ? parameters.get("panAndZoom") : "false");
		String zoomButtons = Escape.html(parameters.containsKey("zoomButtons") ? parameters.get("zoomButtons") : "false");
		String startActive = Escape.html(parameters.containsKey("startActive") ? parameters.get("startActive") : "true");
		String useWheel = Escape.html(parameters.containsKey("useWheel") ? parameters.get("useWheel") : "true");
		String IEToolTips = Escape.html(parameters.containsKey("IEToolTips") ? parameters.get("IEToolTips") : "false");
		String linkHoverHighlight = Escape.html(parameters.containsKey("linkHoverHighlight") ? parameters.get("linkHoverHighlight") : "true");
		String linkHighlight = Escape.html(parameters.containsKey("linkHighlight") ? parameters.get("linkHighlight") : "false");
		String minZoom = Escape.html(parameters.containsKey("minZoom") ? parameters.get("minZoom") : "0.1");
		String maxZoom = Escape.html(parameters.containsKey("maxZoom") ? parameters.get("maxZoom") : "10.0");
		if(panAndZoom.equals("false")) {
			zoomButtons = "false";
		}

		// add span around the SVG container (img or object)		
		svgOutput = String.format("<span class='svg-out' panAndZoom='%s' useWheel='%s' startActive='%s' zoomButtons='%s' minZoom='%s' maxZoom='%s' IEToolTips='%s' linkHoverHighlight='%s' linkHighlight='%s'>%s</svg>",
			panAndZoom, useWheel, startActive, zoomButtons, minZoom, maxZoom, IEToolTips, linkHoverHighlight, linkHighlight, svgOutput);

		
		// get the alignment parameter
		String pAlign = Escape.html(parameters.containsKey("align") ? parameters.get("align") : "left");
		
		// align style for the div
		String divStyle = "text-align:left";
		switch(pAlign) {
			case "left": divStyle = "text-align:left"; break;
			case "center": divStyle = "text-align:center"; break;
			case "right": divStyle = "text-align:right"; break;
			case "right with text float": divStyle = "margin-left:10px;margin-bottom:10px;float:right"; break;
			default: break;
		}
		
		// wrap the span in a div for alignment
		svgOutput = String.format("<div style='%s'>%s</div>", divStyle, svgOutput);

		// return the string representation of our SVG
		return svgOutput;
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
	
	
	// find the right ContentEntityObject
	private ContentEntityObject findMyCEO(Map<String, String> parameters, ConversionContext conversionContext) {
		
		// do some init stuff
		ContentEntityObject ceo = null;
		Page page = null;
		
		// find the right page
		// in case the parameter page is not given we take the current entity 
		if(!parameters.containsKey("page")) {
			ceo = conversionContext.getEntity();
		}
		// in case the parameter page is specified
		else {
			// get the parameter page
			String pageParam = parameters.get("page");
			
			// is the "unofficial" parameter "space" given?
			// remark: the JS only sets it in case the space is different from the current space
			if(parameters.containsKey("space")) {
				// get the page using space and page parameter
				String spaceParam = parameters.get("space");
				page = pageMan.getPage(spaceParam, pageParam);
			}
			// in case "space" is not in the list of parameters
			else {
				// we will find the page in our space
				page = pageMan.getPage(conversionContext.getSpaceKey(), pageParam);
			}
		
			// in case we found a page get the entity of it
			if(page != null) {
				ceo = page.getEntity();
			}
		}
		
		// return the ceo we found
		return ceo;
	}
	
	
	// zoom and cut the SVG as required
	private boolean zoomAndCut(String pScaleX, String pScaleY, String left, String top, String right, String bottom, boolean pdfOutput, String pngUpscale) {
		
		// get the attributes of the SVG element
		String iViewBox = svgE.getAttribute("viewBox");
		String iHeight = svgE.getAttribute("height");
		String iWidth = svgE.getAttribute("width");
		
		/* bug report #1: some SVG sources define height and width in the style attribute
		in case style contains those values we override the existing values and
		delete height and width from the style attribute*/
		String style = svgE.getAttribute("style");
		
		// is the attribute existing?
		if(style.length() > 0) {
			
			// define the regular expressions we need to extract width and height
			final String widthRegex = "(^|;) *width:.*?(;|\\Z)";
			final String heightRegex = "(^|;) *height:.*?(;|\\Z)";
			final String numberRegex = "[0-9.]{1,}";
			
			// get the width out of style
			String interm = regexExtractor(style, widthRegex);
			String dim = regexExtractor(interm, numberRegex);
			if(dim.length() > 0) {
				iWidth = dim;
			}
			
			// get the height out of the style
			interm = regexExtractor(style, heightRegex);
			dim = regexExtractor(interm, numberRegex);
			if(dim.length() > 0) {
				iHeight = dim;
			}
			
			// remove width and height from the style (if they exist)
			style = style.replaceFirst(widthRegex, "");
			style = style.replaceFirst(heightRegex, "");
			style = style.replaceFirst(" *; *\\Z", "");
		}
		
		// remove any existing overflow definition from style and set overflow:hidden
		// overflow:hidden prevents parts of the SVG being drawn outside the viewBox
		style = style.replaceFirst("overflow:.*?(;|\\Z)", "");
		if(style.length() > 0) {
			style = style.concat("; overflow:hidden");
		}
		else {
			style = "overflow:hidden";
		}
		// set the style attribute
		svgE.setAttribute("style", style);
		
		// in case the width and height are not specified but the viewbox we take width and height from the viewbox
		// Adobe Illustrator does such things
		if((iHeight.isEmpty()) && (iWidth.isEmpty()) && (iViewBox.length() > 6)) {
			ArrayList<String> viewBoxItems = new ArrayList<String>(Arrays.asList(iViewBox.split(",? ")));
			if(viewBoxItems.size() == 4) {
				iWidth = viewBoxItems.get(2);
				iHeight = viewBoxItems.get(3);
			}
		}
		
		// only set the viewBox in case it was not defined in the image
		if(iViewBox.isEmpty()) {
			// the viewBox will always be the size of the original image (even when scaled!)
			iViewBox = "0 0 " + convert2Unitless(iWidth) + " " + convert2Unitless(iHeight);
		}
		
		// different units used?
		if((getUnit(pScaleX).equals("px") && getUnit(pScaleY).equals("%")) ||
				(getUnit(pScaleX).equals("%") && getUnit(pScaleY).equals("px"))) {
			errorMsg = i18n.getText("de.edrup.confluence.plugins.svg-out.message.units");
			return false;
		}
		
		// in case of png output and %% we set the 100% to 800px
		if(pdfOutput && pScaleX.contains("%%")) {
			Double percentWidth = 800.0 * Double.parseDouble(getNumber(getNumber(pScaleX))) / 100.0;
			pScaleX = percentWidth.toString() + "px";
		}
				
		// scaling when user wants absolute px rather than %
		// remark: as the subsequent scaleValue calls change width and height again we have to compensate this here
		if(getUnit(pScaleX).equals("px") || getUnit(pScaleY).equals("px")) {
			String widthPx = convert2Unitless(iWidth);
			String heightPx = convert2Unitless(iHeight);
			if(getUnit(pScaleX).equals("px")) {
				Double sX = Double.parseDouble(getNumber(pScaleX)) / Double.parseDouble(widthPx) * 100
						/ Math.abs((Double.parseDouble(getNumber(right)) - Double.parseDouble(getNumber(left))) / 100);
				pScaleX = sX.toString();
			}
			if(getUnit(pScaleY).equals("px")) {
				Double sY = Double.parseDouble(getNumber(pScaleY)) / Double.parseDouble(heightPx) * 100
						/ Math.abs((Double.parseDouble(getNumber(bottom)) - Double.parseDouble(getNumber(top))) / 100);
				pScaleY = sY.toString();
			}
			iWidth = widthPx + "px";
			iHeight = heightPx + "px";
		}
		
		// handle undefined values
		if(pScaleX.equals("undefined") && !pScaleY.equals("undefined")) {
			pScaleX = pScaleY;
		}
		else if (!pScaleX.equals("undefined") && pScaleY.equals("undefined")) {
			pScaleY = pScaleX;
		}
		else if (pScaleX.equals("undefined") && pScaleY.equals("undefined")) {
			pScaleY = pScaleX = "100%";
		}
		
		// up-scale image for png export
		if(pdfOutput) {
			Double withUpscaleX = Double.parseDouble(getNumber(getNumber(pScaleX))) * Double.parseDouble(getNumber(getNumber(pngUpscale))) / 100.0;
			Double withUpscaleY = Double.parseDouble(getNumber(getNumber(pScaleY))) * Double.parseDouble(getNumber(getNumber(pngUpscale))) / 100.0;
			pScaleX = withUpscaleX.toString() + getUnit(pScaleX);
			pScaleY = withUpscaleY.toString() + getUnit(pScaleY);
		}
		
		// scale the size of the image
		iWidth = scaleValue(iWidth, pScaleX, left, right);
		iHeight = scaleValue(iHeight, pScaleY, top, bottom);
		
		// cut the viewBox
		iViewBox = cutViewBox(iViewBox, left, top, right, bottom);
				
		// set the attributes
		// remark: in order to scale in X and Y direction differently we have to set preserveAspectRatio to none
		svgE.setAttribute("width", iWidth);
		svgE.setAttribute("height", iHeight);
		svgE.setAttribute("viewBox", iViewBox);
		svgE.setAttribute("preserveAspectRatio", "none");
		
		// special case %% (% of the available width)
		if(getUnit(pScaleX).equals("%%") && !pdfOutput) {
			svgE.setAttribute("width", "100%");
			svgE.removeAttribute("height");
		}
		
		return true;
	}
	
	
	// cut the viewBox so that we only see a part of the image
	public String cutViewBox(String viewBox, String left, String top, String right, String bottom) {
		
		// split the viewBox
		ArrayList<String> viewBoxItems = new ArrayList<String>(Arrays.asList(viewBox.split(",? ")));
		
		// in case the viewBox is appropriate (containing 4 elements)
		if(viewBoxItems.size() == 4) {
			
			// calculate the viewBox items
			Double xmin = Double.parseDouble(viewBoxItems.get(0));
			Double ymin = Double.parseDouble(viewBoxItems.get(1));
			Double width = Double.parseDouble(viewBoxItems.get(2));
			Double height = Double.parseDouble(viewBoxItems.get(3));
			
			// calculate the cut viewBox items
			Double xminC = xmin + width * Double.parseDouble(getNumber(left)) / 100;
			Double yminC = ymin + height * Double.parseDouble(getNumber(top)) / 100;
			Double widthC = width * Math.abs(Double.parseDouble(getNumber(right)) - Double.parseDouble(getNumber(left))) / 100;
			Double heightC = height * Math.abs(Double.parseDouble(getNumber(bottom)) - Double.parseDouble(getNumber(top))) / 100;
			
			// and put them together to a string
			viewBox = xminC.toString() + " " + yminC.toString() + " " + widthC.toString() + " " + heightC.toString();
		}
		
		// return the cut viewBox
		return viewBox;
	}
	
	
	// link replacement and adding of tool tips
	private void linkAndTooltipHandler(String bodyContent, String pageUrl) {
		
		Node linkNode = null;
		Element linkElement = null;
		
		// get all links form the SVG
		NodeList linkNodes = svgE.getElementsByTagName("a");
		
		// define all the patterns we need
		Pattern pLink = Pattern.compile("<a.*?>.*?</a>");
		Pattern pTTEnd = Pattern.compile("</p>|<br|<a|$");
		Pattern phref = Pattern.compile("href *= *\".*?\"");
		Pattern pLinkText = Pattern.compile("(?<=>).*?(?=</a>)");
		
		// start the link matcher
		Matcher mLink = pLink.matcher(bodyContent);

		// add the target _top to all links which do not specify a target
		for(int n = 0; n < linkNodes.getLength(); n++) {
			linkNode = linkNodes.item(n);
			if(linkNode.getNodeType() == Node.ELEMENT_NODE) {
				linkElement = (Element) linkNode;
				if(!linkElement.hasAttribute("target")) {
					linkElement.setAttribute("target", "_top");
				}
			}
		}

		// as long as we find links
		while(mLink.find()) {
			
			// the complete link
			String link = mLink.group();
			
			// extract the href out of the link
			String href = "none";
			Matcher mhref = phref.matcher(link);
			if(mhref.find()) {
				href = mhref.group().replaceFirst("href *= *\"", "").replaceFirst("\"", "");
			}
			
			// ensure that we have the right URL to escape the object (which is in a different context)
			if(href.startsWith("#")) {
				href = settingsMan.getGlobalSettings().getBaseUrl() + pageUrl + href;
			}
			else if(href.startsWith("/")) {
				String urlWithoutContext = settingsMan.getGlobalSettings().getBaseUrl();
				try {
					urlWithoutContext = urlWithoutContext.replace(new URL(urlWithoutContext).getPath(), "");
				}
				catch(Exception e) {}
				href = urlWithoutContext + href;
			}
			
			// extract the link text out of the link
			String linkText = "";
			Matcher mLinkText = pLinkText.matcher(link);
			if(mLinkText.find()) {
				linkText = HtmlUtils.htmlUnescape(mLinkText.group());
			}
			
			// extract the tool tip
			String potentialTT = bodyContent.substring(mLink.end()).trim();
			String tooltip = "none";
			if(potentialTT.startsWith("tooltip")) {
				Matcher mTTEnd = pTTEnd.matcher(potentialTT);
				if(mTTEnd.find()) {
					tooltip = potentialTT.substring(0, mTTEnd.start()).replaceFirst("tooltip *= *", "").trim();
				}
			}
			
			// loop through all the link nodes
			for(int n = 0; n < linkNodes.getLength(); n++) {
				linkNode = linkNodes.item(n);
				
				// we only continue in case the node is an element (should always be the case)
				if(linkNode.getNodeType() == Node.ELEMENT_NODE) {
					linkElement = (Element) linkNode;
					
					// check whether it contains the xlink:href attribute
					if(linkElement.hasAttribute("xlink:href")) {
						// if yes is it our link to be replaced?
						if(linkElement.getAttribute("xlink:href").equals(linkText)) {
							// replace the link
							linkElement.setAttribute("xlink:href", href);
							
							// did the user specify a tool tip?
							if(!tooltip.equals("none")) {
								// set the xlink:title attribute to the tool tip
								linkElement.setAttribute("xlink:title", tooltip);
							}
						}
					}
					
					// the same for href without xlink (not sure whether this exists)
					if(linkElement.hasAttribute("href")) {
						if(linkElement.getAttribute("href").equals(linkText)) {
							linkElement.setAttribute("href", href);
							if(!tooltip.equals("none")) {
								linkElement.setAttribute("xlink:title", tooltip);
							}
						}
					}					
				}
			}
		}	
	}
	

	// get the number part of a string consisting of number and unit
	private String getNumber(String value) {
		return regexExtractor(value, "[-0-9.]{1,}");
	}
	
	
	// get the unit part of a string consisting of number and unit
	private String getUnit(String value) {
		return regexExtractor(value, "[^-0-9. ]{1,}");
	}
	
	
	// scale a value consisting of number and unit by a percent value and the cut edges
	private String scaleValue(String value, String scale, String edge1, String edge2) throws NumberFormatException {
		Double d = Double.parseDouble(getNumber(value))
				* Double.parseDouble(getNumber(scale))
				* Math.abs(Double.parseDouble(getNumber(edge2))
						- Double.parseDouble(getNumber(edge1))) / (100 * 100);
		return d.toString() + getUnit(value);
	}
	
	
	// scale a value consisting of number and unit to a number without any unit following the SVG standard
	private String convert2Unitless(String value) throws NumberFormatException {
		String number = getNumber(value);
		String unit = getUnit(value);
		Double number_double = Double.parseDouble(number);
		switch(unit) {
			case "in": number_double *= 90.0; break;
			case "px": break;
			case "mm": number_double *= 3.54; break;
			case "cm": number_double *= 35.4; break;
			case "pc": number_double *= 15.0; break;
			case "pt": number_double *= 1.25; break;
			default: break;
		}
		return number_double.toString();
	}
	
	
	// extract a substring based on the regex given
	private String regexExtractor(String s, String regex) {
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(s);
		if(m.find()) {
			return m.group(0);
		}
		else {
			return "";
		}
	}
	
	
	// convert an input stream to a document
	private void streamToDocument(InputStream svg) throws SAXException, IOException, ParserConfigurationException {
		// set up a document builder factory
		// remark: the basic principle factory, builder and then parse comes from various sources in the interner
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		
		// disable some DTD validation features as they will cause an exception when the server is not connected to the internet
		// remark: the feature disabling worked so far; other sources indicate to set a new entitiyResolver in the factory which returns an empty stream
		dbFactory.setValidating(false);
		dbFactory.setNamespaceAware(false);
		dbFactory.setFeature("http://xml.org/sax/features/namespaces", false);
		dbFactory.setFeature("http://xml.org/sax/features/validation", false);
		dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
		dbFactory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
		
		// security stuff
		dbFactory.setExpandEntityReferences(false);
		//dbFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
		dbFactory.setFeature("http://xml.org/sax/features/external-general-entities", false);
		dbFactory.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
		dbFactory.setXIncludeAware(false);
		
		// set up the document builder from the factory
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		
		svgD = dBuilder.parse(svg);
	}
	
	
	// transfer the DOM to a string
	private boolean domToString() {
		StringWriter svgWriter = null;
		try
		{
			// this comes from the Internet - most people seem to do it like this
			DOMSource domSource = new DOMSource(svgD);
			svgWriter = new StringWriter();
			StreamResult result = new StreamResult(svgWriter);
			TransformerFactory tf = TransformerFactory.newInstance();
			try {
				tf.setAttribute(XMLConstants.FEATURE_SECURE_PROCESSING, true);
			}
			catch(IllegalArgumentException e) {
				log.info("Could not set XMLConstants.FEATURE_SECURE_PROCESSING");
			}
			try {
				tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
			}
			catch(IllegalArgumentException e) {
				log.info("Could not set XMLConstants.ACCESS_EXTERNAL_DTD");
			}
			try {
				tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
			}
			catch(IllegalArgumentException e) {
				log.info("Could not set XMLConstants.ACCESS_EXTERNAL_STYLESHEET");
			}
			Transformer transformer = tf.newTransformer();
			transformer.transform(domSource, result);
			svgS = svgWriter.toString();
			return true;
		}
		catch(TransformerException e)
		{
			errorMsg = e.toString();
			return false;
		}
	}
	
	
	// generate a document from the given attachment
	private boolean documentFromAttachment(Map<String, String> parameters, ConversionContext conversionContext) {
		
		// get the right content entity object
		ContentEntityObject ceo = findMyCEO(parameters, conversionContext);
		if(ceo == null) {
			errorMsg = i18n.getText("de.edrup.confluence.plugins.svg-out.message.pageNotFound");
			return false;
		}
		
		// check whether the user has access rights to the page
		if(!canView(ceo)) {
			errorMsg = i18n.getText("de.edrup.confluence.plugins.svg-out.message.accessRights");
			return false;
		}
		
		// get the attachment
		// remark: the parameter is called "name" as this is required by the javascript provided by Atlassian
		Attachment svgAttachment = null;
		svgAttachment = attachmentMan.getAttachment((ContentEntityObject) ceo.getLatestVersion(), parameters.get("name"));
		if(svgAttachment == null) {
			errorMsg = i18n.getText("de.edrup.confluence.plugins.svg-out.message.attachementNotFound");
			return false;
		}
		
		// create a document (DOM) from the attachment data
		try {
			
			// uncompressed svg
			if(svgAttachment.getFileExtension().equals("svg")) {
				streamToDocument(attachmentMan.getAttachmentData(svgAttachment));
			}
			
			// compressed svgz
			else if (svgAttachment.getFileExtension().equals("svgz")) {
				GZIPInputStream gzi = new GZIPInputStream(attachmentMan.getAttachmentData(svgAttachment));
				streamToDocument(gzi);
			}
			
			// neither svg nor svgz
			else {
				errorMsg = i18n.getText("de.edrup.confluence.plugins.svg-out.message.fileExtension");
				return false;
			}
		}
		catch(IOException | ParserConfigurationException | SAXException e) {
			errorMsg = e.toString();
			return false;
		}
		
		return true;
	}
	
	
	// check the user's access rights (Confluence version >= 5.2)
	private boolean canView(ContentEntityObject ceo) {
		try {
			ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
			return permissionMan.hasPermission(confluenceUser, Permission.VIEW, ceo);
		}
		// for Confluence version prior to 5.2 we simply return true
		catch(NoSuchMethodError e) {
			return true;
		}
	}
	
	
	// convert the body to a document
	private boolean documentFromBody(String bodyContent) {
					
		// check whether the body contains an SVG
		errorMsg = "none";
		if(!bodyContent.toLowerCase().contains("<svg") || !bodyContent.toLowerCase().contains("/svg>")) {			
			errorMsg = i18n.getText("de.edrup.confluence.plugins.svg-out.message.noSVG") + " Details: " + errorMsg;
			return false;
		}
		
		// remove parts in front and after the <svg> tags
		bodyContent = bodyContent.substring(bodyContent.indexOf("<svg"), bodyContent.indexOf("</svg>") + 6);
		
		// convert the result into a stream
		InputStream stream = new ByteArrayInputStream(bodyContent.toString().getBytes(StandardCharsets.UTF_8));
		
		// generate the document from the stream
		try {
			streamToDocument(stream);
		}
		catch(IOException | ParserConfigurationException | SAXException e) {
			errorMsg = e.toString();
			return false;
		}
		
		return true;
	}
		
	
	// convert our SVG to base64 encoded image
	private String svg2Img(boolean border, String pScaleX, String rasterResolution) {
		String imgData = "data:image/svg+xml;base64," + Base64.getEncoder().encodeToString(svgS.getBytes());
		String imgStyle = "";
		if(border) {
			imgStyle="border:1px solid black";
		}
		if(!getUnit(pScaleX).equals("%%")) {
			return "<img width='" + pScaleX + "' style='" + imgStyle +  "' src='" + imgData + "'/>";
		}
		else {
			return "<img class='confluence-embedded-image' width='" + getNumber(pScaleX) + "%' style='" + imgStyle +  "' src='" + imgData + "'/>";			
		}
	}

		
	// get all MacroDefinitions of the provided content
	private List<MacroDefinition> getMacroDefs(String content, ConversionContext conversionContext) throws MacroExecutionException {
		// generate a list of MacroDefinitions from the given content
		final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
		try
		{
			xhtmlUtils.handleMacroDefinitions(content, conversionContext, new MacroDefinitionHandler()
			{
				@Override
				public void handle(MacroDefinition macroDefinition) {
					macros.add(macroDefinition);
				}
			});
		}
        catch (XhtmlException e) {
        	throw new MacroExecutionException(e);
        }
		
		return macros;
	}
	
	
	// check of Scroll PDF is active
	private boolean isScrollPDFActive() {
		try {
			return pluginAcc.isPluginEnabled("com.k15t.scroll.scroll-pdf");
		}
		catch(Exception e) {
			return false;
		}
	}
}