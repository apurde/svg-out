package de.edrup.confluence.plugins;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

public class SVGOutConfigurationAction extends ConfluenceActionSupport {
	
	private static final long serialVersionUID = 42L;

	private final BandanaManager bandanaMan;
	
	@Inject
	public SVGOutConfigurationAction(@ComponentImport BandanaManager bandanaMan) {
		this.bandanaMan = bandanaMan;
	}
	
	@Override
	public String execute() throws Exception {
		
		super.execute();
        
		HttpServletRequest request = getCurrentRequest();
        SVGOutConfiguration configuration = new SVGOutConfiguration();
        
        // in case the request contains our first input field we assume that we have a POST call here
        if(request.getParameter("whitelist") != null) {
        	
        	// security check active
        	if(request.getParameter("checkActive") != null) {
        		 configuration.setActive(true);
        	}
        	else {
        		 configuration.setActive(false);
        	}
        	
        	// phantomLink
        	if(request.getParameter("phantomLink") != null) {
        		configuration.setPhantomLink(request.getParameter("phantomLink"));
	       	}
	       	else {
	       		 configuration.setPhantomLink("");
	       	}
        	
        	// whiteRegEx
        	if(request.getParameter("whiteRegEx") != null) {
        		configuration.setWhiteRegEx(request.getParameter("whiteRegEx"));
	       	}
	       	else {
	       		 configuration.setWhiteRegEx("");
	       	}
        	
        	// whiteListGroup
        	if(request.getParameter("whiteListGroup") != null) {
        		configuration.setWhiteListGroup(request.getParameter("whiteListGroup"));
	       	}
	       	else {
	       		 configuration.setWhiteListGroup("");
	       	}

        	// whitelist
	        configuration.setWhitelist(request.getParameter("whitelist"));
	        
	        // save the configuration
	        configuration.storeConfiguration(bandanaMan);
        }
        
        return SUCCESS;
    }
	
	public SVGOutConfiguration getConfiguration() {
		SVGOutConfiguration configuration = new SVGOutConfiguration();
		configuration.readConfiguration(bandanaMan);
		return configuration;
	}
}
