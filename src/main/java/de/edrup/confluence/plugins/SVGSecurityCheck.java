package de.edrup.confluence.plugins;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SVGSecurityCheck {
	
	// Sources:
	// https://www.owasp.org/index.php/XSS_Filter_Evasion_Cheat_Sheet
	// https://www.owasp.org/images/0/03/Mario_Heiderich_OWASP_Sweden_The_image_that_called_me.pdf
	// http://www.w3.org/TR/SVG/interact.html#SVGEvents
		
	private static final String badRegEx[] = {"<script", "javascript:", "&#[0-9]+", "&#x[0-9abcdef]+",
		"onfocusin", "onfocusout", "onactivate", "onclick", "onmousedown", "onmouseup", "onmouseover",
		"onmousemove", "onmouseout", "onload", "onunload", "onabort", "onerror", "onresize", "onscroll",
		"onzoom", "onbegin", "onend", "onrepeat", "onkey", "ondblclick", "\\bon[a-z]*=" };
	private static final String stripper = "[^ a-z0-9.,;:=<>+\"'\\-\\/#\\*\\(\\)&]";
	
	
	public SVGSecurityCheck() {
	}
	
	
	// calculate the MD5 checksum from the given SVG (as String) and return a hex string (legacy)
	public String MD5Calc(String svg) {
		byte[] bytesOfSVG;
		try {
			bytesOfSVG = svg.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			return e.toString();
		}
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] md5 = md.digest(bytesOfSVG);
			return bytesToHex(md5);
		} catch (NoSuchAlgorithmException e) {
			return e.toString();
		}
	}
	
	
	// calculate the SHA3-256 checksum from the given SVG (as String) and return a hex string
	public String SHA256Calc(String svg) {
		byte[] bytesOfSVG;
		try {
			bytesOfSVG = svg.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e) {
			return e.toString();
		}
		try {
			MessageDigest md = MessageDigest.getInstance("SHA3-256");
			byte[] sha = md.digest(bytesOfSVG);
			return bytesToHex(sha);
		} catch (NoSuchAlgorithmException e) {
			return e.toString();
		}
	}
	
		
	// convert a byte array into a hex string
	private String bytesToHex(byte[] in) {
	    final StringBuilder builder = new StringBuilder();
	    for(byte b : in) {
	        builder.append(String.format("%02x", b));
	    }
	    return builder.toString();
	}
	
	
	// remove all characters from the SVG which we do not need for evaluation
	private String stripSVG(String svg) {
		return svg.toLowerCase().replaceAll(stripper, "");
	}
	
	
	// remove all white listed RegEx
	private String stripWhiteRegEx(String svg, String whiteRegEx) {
		String[] regExs = whiteRegEx.split(";");
		for(String regEx : regExs) {
			if(!regEx.isEmpty()) {
				svg = svg.replaceAll(regEx, "");
			}
		}
		return svg;
	}
	
	
	// check whether the SVG given as string contains any of the bad regular expressions
	// a return value false means that the check was negative!
	public boolean doCheck(String svg, String whiteRegEx) {
		
		// remove all white-listed parts
		svg = stripWhiteRegEx(svg, whiteRegEx);
		
		// prepare the SVG
		svg = stripSVG(svg);
		
		// loop through all bad regular expressions
		for(int r = 0; r < badRegEx.length; r ++) {
			Pattern p = Pattern.compile(badRegEx[r], Pattern.DOTALL);
			Matcher m = p.matcher(svg);
			// positive match => we might not be safe
			if(m.find()) {
				return false;
			}
		}
		return true;
	}
}
