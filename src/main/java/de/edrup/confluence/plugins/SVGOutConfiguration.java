package de.edrup.confluence.plugins;

import java.io.Serializable;
import java.lang.reflect.Field;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;

public class SVGOutConfiguration implements Serializable {
	
	// this is the first version of our configuration class
	private static final long serialVersionUID = 1L;
	
	// the relevant configuration parameters
	private boolean active;
	private String whitelist;
	private String phantomLink;
	private String whiteRegEx;
	private String whiteListGroup;
	
	private static final Logger log = LoggerFactory.getLogger(SVGOutConfiguration.class);
	
	// set and get methods for external access
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public boolean getActive() {
		return active;
	}

	public void setWhitelist(String whitelist) {
		this.whitelist = whitelist;
	}
	
	public String getWhitelist() {
		return whitelist;
	}
	
	public void setPhantomLink(String phantomLink) {
		this.phantomLink = phantomLink;
	}
	
	public String getPhantomLink() {
		return phantomLink;
	}
	
	public void setWhiteRegEx(String whiteRegEx) {
		this.whiteRegEx = whiteRegEx;
	}
	
	public String getWhiteRegEx() {
		return whiteRegEx;
	}
	
	public void setWhiteListGroup(String whiteListGroup) {
		this.whiteListGroup = whiteListGroup;
	}
	
	public String getWhiteListGroup() {
		return whiteListGroup;
	}
	
	// store the configuration in Bandana
	public void storeConfiguration(BandanaManager bandanaMan) {
		bandanaMan.setValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.svg-out.configuration.v1", this);
	}
	
	// read the configuration from Bandana or initialize it with default values in case it does not exist
	public void readConfiguration(BandanaManager bandanaMan) {
		
		SVGOutConfiguration shadow = null;
		try {
			shadow = (SVGOutConfiguration) bandanaMan.getValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.svg-out.configuration.v1");
		}
		catch(Exception e1) {
			log.error("Loading the configuration the normal way failed: {}", e1.toString());
			try {
				shadow = readFromObject(bandanaMan.getValue(new ConfluenceBandanaContext(), "de.edrup.confluence.plugins.svg-out.configuration.v1"));
			}
			catch(Exception e2) {
				log.error("Loading the configuration the alternative way failed: {}", e2.toString());
			}
		}
		
		// no configuration stored in Bandana => init with default values
		if(shadow == null) {
			active = true;
			whitelist = "";
			phantomLink = "";
			whiteRegEx = "";
			whiteListGroup = "";
		}
		// configuration found
		else {
			active = shadow.getActive();
			whitelist = shadow.getWhitelist();
			phantomLink = shadow.getPhantomLink();
			whiteRegEx = shadow.getWhiteRegEx();
			whiteListGroup = shadow.getWhiteListGroup();
		}	
		
		whiteRegEx = (whiteRegEx == null) ? "" : whiteRegEx;
		whiteListGroup = (whiteListGroup == null) ? "" : whiteListGroup;
	}
	
	
	// this in the next function had to be introduced as sometimes Confluence uses different class loaders when setting and getting the settings
	private SVGOutConfiguration readFromObject(Object o) {
		SVGOutConfiguration shadow = new SVGOutConfiguration();
		shadow.setActive(getFieldValue(o, "active") != null ? (Boolean) getFieldValue(o, "active") : true);
		shadow.setPhantomLink(getFieldValue(o, "phantomLink") != null ?  (String) getFieldValue(o, "phantomLink") : "");
		shadow.setWhitelist(getFieldValue(o, "whitelist") != null ?  (String) getFieldValue(o, "whitelist") : "");
		shadow.setWhiteRegEx(getFieldValue(o, "whiteRegEx") != null ? (String) getFieldValue(o, "whiteRegEx") : "");
		shadow.setWhiteListGroup(getFieldValue(o, "whiteListGroup") != null ? (String) getFieldValue(o, "whiteListGroup") : "");
		return shadow;
	}
	
	
	private Object getFieldValue(Object o, String fieldName) {
		try  {
			Field f = o.getClass().getDeclaredField(fieldName);
			f.setAccessible(true);
			return f.get(o);
		}
		catch(Exception e) {
			return null;
		}
	}	
}
