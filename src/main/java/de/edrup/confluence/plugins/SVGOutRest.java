package de.edrup.confluence.plugins;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.annotations.security.AnonymousSiteAccess;
import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

@Path("/")
public class SVGOutRest {
	
	private final BandanaManager bandanaMan;
	private SVGObjectCache svgObjectCache;
	private final PermissionManager permissionMan;
	private final UserAccessor userAcc;

	@Inject
	public SVGOutRest(@ComponentImport BandanaManager bandanaMan, SVGObjectCache svgObjectCache,
		@ComponentImport PermissionManager permissionMan, @ComponentImport UserAccessor userAcc) {
		this.bandanaMan = bandanaMan;
		this.svgObjectCache = svgObjectCache;
		this.permissionMan = permissionMan;
		this.userAcc = userAcc;
	}
	
	
	@GET
	@AnonymousSiteAccess
    @Produces({MediaType.APPLICATION_SVG_XML})
	@Path("/getsvg")
    public Response getSVG(@QueryParam("id") String id) {
		return Response.ok(svgObjectCache.get(id)).cacheControl(getImageCacheControl()).type("image/svg+xml").build();
    }

	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Path("/whitelist")
    public Response whiteList(@QueryParam("sha") String sha, @QueryParam("token") String token) {
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		SVGOutConfiguration conf = new SVGOutConfiguration();
		conf.readConfiguration(bandanaMan);
		if((user != null) && svgObjectCache.isTokenValid(token, sha) && (permissionMan.isSystemAdministrator(user) || userAcc.getGroupNames(user).contains(conf.getWhiteListGroup()))) {		
			conf.setWhitelist(conf.getWhitelist() + "; " + user.getName() + ":" + sha);
			conf.storeConfiguration(bandanaMan);
			return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
		}
		else {
			return Response.status(Status.FORBIDDEN).build();
		}
    }
	
	
	private CacheControl getNoStoreNoCacheControl() {
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		cc.setNoStore(true);
		cc.setMustRevalidate(true);
		return cc;
	}
	
	
	private CacheControl getImageCacheControl() {
		CacheControl cc = new CacheControl();
		cc.setMaxAge(2592000);
		return cc;
	}
}
